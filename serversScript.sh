#!bin/bash

apt update
apt upgrade -y

#install kolibri
apt install wget -y
wget https://learningequality.org/r/kolibri-deb-latest
dpkg -i kolibri-deb-latest
#Start kolibri
kolibri start

