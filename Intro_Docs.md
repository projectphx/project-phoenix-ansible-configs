# Let's build a Mesh!

Welcom to Project Phoenix's mesh table. Feel free to follow this guide to easily
get connected. Once you're online you can start playing around with the typology, 
build a distributed application, or play some games!

#### **If you would like to use your personal machine to get connected** 

- Apart from the utilities required for easy network configuration (Eg. batctl).
  The changes made to your wireless interface _should not_ persist after
  rebooting your machine. However, we do not take any responsibility for
  whatever happens to your equipment. 
**Use your own equiptment at YOUR OWN RISK!**. 

- At this time, we have not tested Microsoft Windows, however that doesn't mean
  you can't attempt to get onto the mesh with your OS. Let one of the
  Project Phoenix Volunteers know if you do succeed!

- Questions and concerns can be addressed directly by the Project Phoenix
  volunteers, please don't be afraid to ask for help if you get stuck!!!

Let's get started!

- - -  
### Log onto a laptop:
- - -  

  - Username: **azbi** 
  - Password: **ProjPX**
  - The password also will be used for `sudo`

- - -  
### Set up the system
- - -

  **Step 1**. Connect to the wifi.

  **Step 2**. Update your system by doing the following:
  Open a terminal (terminal icon, start menu, etc.. There are various options for how) and follow along.

  ```
  $ sudo apt update
  ```

  **Step 3**. Install the necessary software:

  ```
  sudo apt install batctl
  ```
  This one may ask you for confirmation to install. Please do.

- - -
### Connect to the mesh!
- - -
  
  Start by noting your "network interface" names are.
  To view all available network interfaces on the system, use the following command.

  ```
  $ ip link 
  ```

  The output from this command will show you the currently available interfaces. We'll be focusing on the wireless one (starts with a "w").
  
  Example output:

  ```
  1: lo: <BROADCAST,MULTICAST> mtu 1500 qdisc mq state DOWN mode DEFAULT
  group default qlen 1000
      link/ether 8d:c6:83:59:45:0c brd ff:ff:ff:ff:ff:ff

  2: enp5s0: <BROADCAST,MULTICAST> mtu 1500 qdisc mq state DOWN mode DEFAULT
  group default qlen 1000
      link/ether b4:6d:24:59:45:0c brd ff:ff:ff:ff:ff:ff

  3: wlo1: <BROADCAST,MULTICAST> mtu 1500 qdisc mq state DOWN mode DEFAULT
  group default qlen 1000
      link/ether a9:6d:83:59:45:0c brd ff:ff:ff:ff:ff:ff
  ```
  (Thus, our focus is wlo1 in this case)

- - -
### Set up the wireless NIC for mesh networking.
- - -

  This step allows us to enable "ad-hoc" mode on the interface. Ad-hoc mode allows the Network Interface Card (NIC) to talk to other ad-hoc devices on the same network.

  ```
  $ sudo iw wlo1 del
  $ sudo iw phy phy0 interface add wlo1 type ibss
  $ sudo ip link set up mtu 1560 dev wlo1
  ```
  
  Set the NIC to communicate using the mesh network.
 
  ```
  $ sudo iw dev wlo1 ibss join mesh 2412 HT20 fixed-freq
  ```

  Not all devices need this, but to be sure, use this to load the low level software allowing us to use B.A.T.M.A.N.

  ```
  $ sudo modprobe batman-adv
  ```

  The mesh software needs to know which interface to use for sending/recieving to traffic. 

  ```
  $ sudo batctl if add wlo1
  ```
  
- - -
### Set up bat0 
- - -

  The interface for communicating with through B.A.T.M.A.N. should now be viewable by running `ip link` as bat0.

  Example output of `ip link`
  ```
  1: lo: <BROADCAST,MULTICAST> mtu 1500 qdisc mq state DOWN mode DEFAULT
  group default qlen 1000
      link/ether 8d:c6:83:59:45:0c brd ff:ff:ff:ff:ff:ff

  2: enp5s0: <BROADCAST,MULTICAST> mtu 1500 qdisc mq state DOWN mode DEFAULT
  group default qlen 1000
      link/ether b4:6d:24:59:45:0c brd ff:ff:ff:ff:ff:ff

  3: wlo1: <BROADCAST,MULTICAST> mtu 1500 qdisc mq state DOWN mode DEFAULT
  group default qlen 1000
      link/ether a9:6d:83:59:45:0c brd ff:ff:ff:ff:ff:ff

  3: wlo1: <BROADCAST,MULTICAST> mtu 1500 qdisc mq state DOWN mode DEFAULT
  group default qlen 1000
      link/ether a9:6d:83:59:45:0c brd ff:ff:ff:ff:ff:ff
  ```

  bat0 may be "down" (off), as in the example.
  To bring bat0 up (on), use the following command: 

  ```
  $ sudo ip link set up dev bat0
  ```

- - -
### Check to see that everything is working!
- - -

  Let's see if everything is working.

  ```
  $ sudo batctl n
  ```

  You should see a list of other mesh nodes currently connected. 
  
  **Example Output:**

  ```
  $ batctl n

  [B.A.T.M.A.N. adv 2017.4, MainIF/MAC: wlan0/b4:6d:83:59:45:0c
  (bat0/ba:8f:fd:28:dd:bb BATMAN_IV)]
  IF             Neighbor              last-seen
         wlan0   b4:6d:83:59:36:ee    0.084s
  ```

- - -
### Obtaining an Internet Protocol (IP) Address 
- - -

  The interface may be up to talk on a B.A.T.M.A.N mesh; however, we still need to get an IP address to work with most software to access network resources.

  ```
  $ sudo dhclient -4
  ```

  After asking for an IP, use the `ip addr` command to confirm you're able communicate.

  ```
  $ ip addr
  
    1: lo: <BROADCAST,MULTICAST> mtu 1500 qdisc mq state DOWN mode DEFAULT
  group default qlen 1000
      link/ether f9:6d:5b:59:45:66 brd ff:ff:ff:ff:ff:ff

  2: enp5s0: <BROADCAST,MULTICAST> mtu 1500 qdisc mq state DOWN mode DEFAULT
  group default qlen 1000
      link/ether f9:6d:5b:59:45:66 brd ff:ff:ff:ff:ff:ff

  3: wlo1: <BROADCAST,MULTICAST> mtu 1500 qdisc mq state DORMANT mode DEFAULT
  group default qlen 1000
      link/ether bc:8f:fd:28:dd:9b brd ff:ff:ff:ff:ff:ff

  4: bat0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state
  UNKNOWN group default qlen 1000
      link/ether ba:8f:fd:28:dd:bb brd ff:ff:ff:ff:ff:ff
      inet 10.27.0.27/24 brd 10.27.0.255 scope global bat0
          valid_lft forever preferred_lft forever
  ```

  If everything was set up correctly you should now be able to talk to other devices on the mesh network! 

--------------------------------------------------------------------------------
### Additional Resources
Interested in mesh networking, check out these websites and make something

- https://www.open-mesh.org/projects/batman-adv/wiki/Doc-overview
- https://en.wikipedia.org/wiki/Mesh_networking
- http://www.meshnetworks.com/
- http://privateseabass.com

## Meshing in Phoenix!

We're a group of volunteers working toward building our own mesh in Phoenix, you
can find out more by visiting our site:

```
https://azblockchain.org/project-phoenix/
```
